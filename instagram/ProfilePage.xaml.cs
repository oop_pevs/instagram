﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace instagram
{

    public partial class ProfilePage : ContentPage
    {
        public ProfilePage(OneUserProfile userProfile)
        {
            InitializeComponent();

            var profilePictureSource = new UriImageSource { Uri = new Uri(userProfile.profilePicture) };
            var profilePicture = new Image { Source = profilePictureSource };
            var name = new Label { Text = userProfile.userName, FontSize = 16 };
            var fullName = new Label { Text = "("+userProfile.fullName+")", FontSize = 10 };
            var button = new Button { Text = "Back", FontSize = 20 };
            button.Clicked += Handle_Clicked;


            StackLayout profileStackLayout = new StackLayout
            {
                Children = {
                        profilePicture,
                        name,
                        fullName
                    },
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };


            headerGrid.Children.Add(button, 0, 0);
            headerGrid.Children.Add(profileStackLayout, 0, 1);


            for (int y = 0;  y <= userProfile.numberOfPosts / 3; y++) {
                for (int x = 0; x < 3; x++) {
                    if (y * 3 + x < userProfile.numberOfPosts) {
                        var imageSource = new UriImageSource { Uri = new Uri("http://lorempixel.com/400/400/") };
                        imageSource.CachingEnabled = false;
                        var image = new Image { Source = imageSource };
                        grid.Children.Add(image, x, y);
                    }
                }
            }
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            App.Current.MainPage = new MainPage();
        }

    }
}
