﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace instagram
{
    public class OneUserProfile
    {
        public string userName { get; set; }
        public string fullName { get; set; }
        public string profilePicture { get; set; }
        public int numberOfPosts { get; set; }
    }


    public partial class MainPage : ContentPage
    {
        StackLayout parent;
        List<OneUserProfile> userProfiles = JsonConvert.DeserializeObject<List<OneUserProfile>>(File.ReadAllText(@"/Users/eriklaco/Projects/instagram/instagram/data.json"));

        public MainPage()
        {
            InitializeComponent();
            Random rnd = new Random();

            // Generate random 20 posts
            for (int i = 0; i < 20; i++) {
                int userId = rnd.Next(0, userProfiles.Count);

                var button = new Button { Text = userProfiles[userId].userName, BindingContext = userProfiles[userId] };
                button.Clicked += Handle_Clicked;

                var profilePictureSource = new UriImageSource { Uri = new Uri(userProfiles[userId].profilePicture) };
                var profilePicture = new Image { Source = profilePictureSource };

                var imageSource = new UriImageSource { Uri = new Uri("http://lorempixel.com/1200/1200/fashion/") };
                imageSource.CachingEnabled = false;
                var image = new Image { Source = imageSource };


                StackLayout myStackLayout = new StackLayout
                {
                    Children = {
                        profilePicture,
                        button
                    },
                    Orientation = StackOrientation.Horizontal,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };


                parent = stackLayout;
                parent.Children.Add(myStackLayout);
                parent.Children.Add(image);
            }

        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            OneUserProfile userProfile = (sender as Button).BindingContext as OneUserProfile;
            App.Current.MainPage = new ProfilePage(userProfile);
        }
    }
}
